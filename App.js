import React, { useEffect } from 'react';
import 'localstorage-polyfill'; 
import AppContainer from './src/navigations/AppNavigation';
import {  ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloProvider } from '@apollo/react-hooks';

const httpLink = createHttpLink({
  uri: "http://localhost:4004/graphql",
  credentials: "same-origin",
});
const cache = new InMemoryCache();

const authLink = setContext((_, { headers }) => {
  const token = localStorage.getItem('mobile-token-authorization');
  return {
    headers: {
      ...headers,
      authorization: token ? `${token}` : "",
    }
  }
});
const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache,
  onError: (e) => { console.log(e) },
});

//config redux store
/*import { Provider } from 'react-redux';
import configureStore from './src/redux/store';

const store = configureStore();*/

export default function App() {
  return (
    <ApolloProvider client={client}>
      <AppContainer />
    </ApolloProvider>
  );
}