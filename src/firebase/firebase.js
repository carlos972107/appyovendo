import * as firebase from 'firebase';

var firebaseConfig = {
    apiKey: "AIzaSyAcd4u2fgjCvRDVqparF-w8DHm4SirKIPk",
    authDomain: "yovendoapp.firebaseapp.com",
    databaseURL: "https://yovendoapp.firebaseio.com",
    projectId: "yovendoapp",
    storageBucket: "yovendoapp.appspot.com",
    messagingSenderId: "623523909825",
    appId: "1:623523909825:web:070ce6767105af188c4440",
    measurementId: "G-BSCKTW33GD"
};

firebase.initializeApp(firebaseConfig);
export { firebase };