import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: 30,
    width: 105,
    marginTop: 5,
    marginLeft: 10,
    marginBottom: 22,
    marginRight: 10,
    borderRadius: 100,
    borderColor: '#2cd18a',
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center'
    // backgroundColor: '#2cd18a'
  },
  text: {
    fontSize: 14,
    color: '#2cd18a'
  }
});

export default styles;
