import React from "react";
import { ActivityIndicator, StyleSheet, Text, View, Dimensions } from "react-native";
import { theme } from '../../theme';
const { width, height } = Dimensions.get('window');

const Loader = () => (
  <View style={styles.container}>
    <ActivityIndicator size="large" color={theme.colors.primary} />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
    opacity: 1,
    width: '100%',
    height: height,
    justifyContent: "center",
    position: 'absolute',
    zIndex: 100
  }
});

export default Loader;