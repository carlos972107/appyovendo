import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  btnContainer: {
    flex: 1,
    alignItems: 'center',
    padding: 8,
    margin: 11,
  },
  btnIcon: {
    height: 22,
    width: 22,
  }
});

export default styles;
