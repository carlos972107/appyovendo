import React from 'react';
import { TouchableHighlight, Image } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import PropTypes from 'prop-types';
import styles from './styles';

export default class AddButton extends React.Component {
  render() {
    return (
      <TouchableHighlight underlayColor='rgba(73,182,77,1,0.9)' onPress={this.props.onPress} style={styles.btnContainer}>
        <Icon name='plus' size={28} color='#fff' style={{bottom: 5}} />
      </TouchableHighlight>
    );
  }
}

AddButton.propTypes = {
  onPress: PropTypes.func,
  source: PropTypes.number,
  title: PropTypes.string
};