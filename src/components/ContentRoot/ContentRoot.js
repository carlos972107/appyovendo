import React from 'react';
import { theme } from '../../theme';
import { 
    View, 
    KeyboardAvoidingView,
    Text, 
    Image,
    StyleSheet,
    ScrollView
} from 'react-native';

const ContentRoot = (props) => {
    return(
        <View style={styles.background}>
            <View style={styles.container}>
                <Image source={require('../../../assets/logo.png')} style={styles.logo} />
                <Text style={styles.header}>{props.textHeader}</Text>
                {props.children}
            </View>
        </View>
    )
}

export default ContentRoot;

const styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: '#fff',
    },
    container: {
        flex: 1,
        padding: 20,
        width: '100%',
        maxWidth: 340,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        width: 257,
        height: 145,
        marginBottom: 12,
    }, 
    header: {
        fontSize: 26,
        color: theme.colors.primary,
        fontWeight: 'bold',
        paddingVertical: 14,
    },
})