import React, { Component } from 'react';

import { View, Image, TouchableNativeFeedback } from 'react-native';

export default class ShoppinCartButton extends Component {
  render() {
    return (
        <TouchableNativeFeedback {...this.props} >
            <View style={{ flexDirection: 'row' }} >
                <Image
                    source={require('../../assets/icon/shopping-cart.png')}
                    style={{
                        width: 30,
                        height: 30,
                        marginRight: 18,
                    }}
                />
            </View>
        </TouchableNativeFeedback>
    );
  }
}