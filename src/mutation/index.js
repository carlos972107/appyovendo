import { gql } from '@apollo/client';

export const TOKEN_NOTIFICATION = gql`
    mutation createTokenNotification($token: String!){
        createTokenNotification(token: $token){
            _id
        }
    }
`;

export const LOGIN_SELLER = gql`
    mutation loginSeller($email: String!, $password: String!){
        loginSeller(email: $email, password: $password)
    }
`;

export const UPDATE_PASSWORD_SELLER = gql`
    mutation updatePasswordSeller($password: String!){
        updatePasswordSeller(password: $password){
            _id
        }
    }
`;

export const CREATE_SELLER = gql`
    mutation createSeller($businessName: String!, $phone: String!, $mobile: [String!], $email: String!, $password: String!, $address: AddressInput!, $photo_url: String, $role: String!, $state: String!, $initDate: String, $endDate: String, $priority: Int, $paymentMethods: [String]){
        createSeller(data: { businessName: $businessName, phone: $phone, mobile: $mobile, email: $email, password: $password, address: $address, photo_url: $photo_url, role: $role, state: $state, initDate: $initDate, endDate: $endDate, priority: $priority, paymentMethods: $paymentMethods}){
            _id
        }
    }
`;

export const CREATE_CATEGORIE = gql`
    mutation createCategory( $name: String!, $photo_url: String, $description: String ){
        createCategory(data: { name: $name, photo_url: $photo_url, description: $description }){
            _id
            name
            description
            photo_url
        }
    }
`;

export const UPDATE_CATEGORY = gql`
    mutation updateCategory( $name: String!, $photo_url: String, $description: String, $_id: String! ){
        updateCategory(data: { name: $name, photo_url: $photo_url, description: $description }, _id: $_id){
            _id
            name
            description
            photo_url
        }
    }
`;

export const DELETE_CATEGORIE = gql`
    mutation deleteCategory( $_id: String! ){
        deleteCategory( _id: $_id ){
            _id
            name
            description
            photo_url
        }
    }
`;

export const CREATE_PRODUCT = gql`
    mutation createProduct($name: String!, $price: Int!, $description: String, $photo_url: String, $stoktaking: Int!, $visible: Boolean!, $category: String!){
        createProduct(data: { name: $name, price: $price, description: $description, photo_url: $photo_url, stoktaking: $stoktaking, visible: $visible, category: $category }){
            _id
            name
            price
            description
            photo_url
            stoktaking
            visible
            category{
                _id
                name
            }
        }
    }
`;

export const UPDATE_PRODUCT = gql`
    mutation updateProduct($name: String!, $price: Int!, $description: String, $photo_url: String, $stoktaking: Int!, $visible: Boolean, $category: String!, $_id: String!){
        updateProduct( data: { name: $name, price: $price, description: $description, photo_url: $photo_url, stoktaking: $stoktaking, visible: $visible, category: $category }, _id: $_id ){
            _id
            name
            price
            description
            photo_url
            stoktaking
            visible
            category{
                _id
                name
            }
        }
    }
`;

export const DELETE_PRODUCT = gql`
    mutation deleteProduct($_id: String!){
        deleteProduct(_id: $_id){
            _id
            name
            price
            description
            photo_url
            stoktaking
            visible
            category{
                name
            }
        }
    }
`;

export const UPDATE_SELLER = gql`
    mutation updateSeller($businessName: String!, $phot_url: String, $email: String!, $initDate: String, $endDate: String, $phone: String, $mobile: [String!], $paymentMethods: [String], $_id: String!){
        updateSeller(data: { businessName: $businessName, photo_url: $photo_url, email: $email, initDate: $initDate, endDate: $endDate, phone: $phone, mobile: $mobile, paymentMethods: $paymentMethods }, _id: $_id){
            _id
            businessName
            photo_url
            email
            initDate
            endDate
            phone
            mobile
            paymentMethods
        }
    }
`;