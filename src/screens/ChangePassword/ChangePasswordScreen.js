import React, { useState } from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import { TextInput as Input, Button as PaperButton } from 'react-native-paper';
import { theme } from '../../theme';
import Loader from '../../components/Loader/Loader';
import { UPDATE_PASSWORD_SELLER } from '../../mutation';
import { useMutation } from '@apollo/react-hooks';

const ChangePasswordScreen = (props) => {

    const [password, setPassword] = useState('');
    const [rePassword, setRePassword] = useState('');
    const [updatePasswordSeller, { data }] = useMutation(UPDATE_PASSWORD_SELLER);
    const [loader, setLoader] = useState(false);

    const handlerOnPressChangePassword = async () => {
        setLoader(true);
        if(validatePasswordRepeat()){
            const { data } = await updatePasswordSeller({
                variables: { password }
            }).catch(error => {
                setLoader(false);
                Alert.alert('Error!', 'Ocurrio algo inesperado.', [
                    {text: 'Ok'}
                ]);
            });
            if(data.updatePasswordSeller){
                setLoader(false);
                Alert.alert('Modificado!', 'La contraseña a sido modificada con exito.', [
                    {text: 'Ok'}
                ]);
            }
        }
        setPassword('');
        setRePassword('');
    }

    const validatePasswordRepeat = () => {
        if(password && rePassword && password === rePassword) return true;
        else return false;
    }

    return(
        <View style={{margin: 20}}>
            {loader &&
                <Loader />
            }
            <Input
                style={[styles.input, { marginTop: 3 }]}
                selectionColor={''}
                mode="outlined"
                label="Contraseña"
                theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                returnKeyType="done"
                value={password}
                onChangeText={text => setPassword(text)}
                secureTextEntry
            />
            <Input
                style={[styles.input, { marginTop: 3 }]}
                selectionColor={''}
                mode="outlined"
                label="Repetir contraseña"
                theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                returnKeyType="done"
                value={rePassword}
                onChangeText={text => setRePassword(text)}
                secureTextEntry
            />

            <PaperButton
                style={[
                    {
                        width: '100%',
                        marginVertical: 10,
                        backgroundColor: theme.colors.primary
                    }
                ]}
                labelStyle={styles.text}
                mode='contained'
                onPress={() => handlerOnPressChangePassword()}
            >
                Modificar
            </PaperButton>
        </View>
    )
}

ChangePasswordScreen.navigationOptions = {
    title: "¡YO VENDO!, MODIFICAR CONTRASEÑA",
    headerStyle: {
        backgroundColor: theme.colors.primary,
    },
    headerTintColor: '#fff',
};

export default ChangePasswordScreen;

const styles = StyleSheet.create({
    label: {
        color: theme.colors.secondary,
    },
    button: {
        marginTop: 24,
    },
    row: {
        flexDirection: 'row',
        marginTop: 4,
    },
    link: {
        fontWeight: 'bold',
        color: theme.colors.primary,
    },
    input: {
        width: '100%',
        paddingBottom: 8,
        backgroundColor: '#fff',
        borderBottomColor: '#000'
    },
});