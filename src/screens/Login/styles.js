import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');
// orientation must fixed
const SCREEN_WIDTH = width < height ? width : height;

const styles = StyleSheet.create({
    containerView: {
    },
    loginScreenContainer: {
        backgroundColor: '#9C27BO',
        flex: 1,
    },
    logoText: {
        fontSize: 40,
        fontWeight: "bold",
        marginTop: 100,
        marginBottom: 150,
        textAlign: 'center',
        color: '#600EE6'
    },
    loginFormView: {
        flex: 1
    },
    loginFormTextInput: {
        height: 55,
        fontSize: 14,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#eaeaea',
        backgroundColor: '#fafafa',
        paddingLeft: 10,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 5,
        marginBottom: 10,
    },
    loginButton: {
        backgroundColor: '#600EE6',
        width: SCREEN_WIDTH - 30,
        marginLeft: 15,
        borderRadius: 5,
        height: 45,
        marginTop: 10,
    },
    fbLoginButton: {
        width: SCREEN_WIDTH - 30,
        marginLeft: 15,
        height: 45,
        marginTop: 10,
        backgroundColor: '#3897f1',
    },
    footer: {
        textAlign: 'center',
        alignItems: 'center',
        fontSize: 12,
        marginBottom: 15
    },
    row: {
        textAlign: 'center',
        alignItems: 'center',
        marginTop: 6,
    },
    label: {
        color: '#414757',
    },
    link: {
        fontWeight: 'bold',
        color: '#600EE6',
    },
    forgotPassword: {
        width: SCREEN_WIDTH - 20,
        alignItems: 'flex-end',
        marginBottom: 24,
    },
});

export default styles;