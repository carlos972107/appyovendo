import React from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    StyleSheet,
    Alert
} from 'react-native';
import { TextInput as Input, Button as PaperButton } from 'react-native-paper';
import { useMutation } from '@apollo/react-hooks';
import { LOGIN_SELLER } from '../../mutation';
import ContentRoot from '../../components/ContentRoot/ContentRoot';
import { theme } from '../../theme';
import { emailValidatorLogin, validInputValue } from '../../utils';
import Loader from '../../components/Loader/Loader';

const LoginScreen = ({navigation}) => {

    const [email, setEmail] = React.useState({ value: '', error: '' });
    const [password, setPassword] = React.useState({ value: '', error: '' });
    const [loader, setLoader] = React.useState({ value: false });

    const [loginSeller, { data }] = useMutation(LOGIN_SELLER);

    const loginHandle = async () => {
        setLoader({ value: true });
        const validationEmail = emailValidatorLogin(email.value);
        const validationPassword = validInputValue(password.value);
        console.log(validationEmail)
        if(validationEmail.length < 0 || !validationPassword){
            setEmail({ ...email, error: validationEmail });
            setPassword({ ...password, error: validationPassword });
            setLoader({ value: false });
            return;
        }
        let { data } = await loginSeller({ variables: { email: email.value, password: password.value }})
        .catch(error => {
            console.log(error)
            const err = error.graphQLErrors.map(error => error.message);
            setLoader({ value: false });
            Alert.alert('Credenciales incorrectas', err[0], [
                {text: 'Ok'}
            ]);
            return;
        });
        if (data.loginSeller) {
            setLoader({ value: false });
            localStorage.setItem('mobile-token-authorization', data.loginSeller);
            navigation.navigate('RequestOrders');
            return;
        }
    }

    return (
        <ContentRoot textHeader='Bienvenido a ¡Yo Vendo!.'>
            {loader.value && 
                <Loader />
            }
            <View style={styles.containerInput}>
                <Input
                    style={styles.input}
                    selectionColor={''}
                    theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                    mode="outlined"
                    label="Correo electronico"
                    returnKeyType="next"
                    value={email.value}
                    onChangeText={text => setEmail({ value: text, error: '' })}
                    error={!!email.error}
                    errorText={email.error}
                    autoCapitalize="none"
                    autoCompleteType="email"
                    textContentType="emailAddress"
                    keyboardType="email-address"
                />
                {email.error ? <Text style={styles.error}>{email.error}</Text> : null}
            </View>
            <View style={styles.containerInput}>
                <Input
                    style={styles.input}
                    selectionColor={''}
                    theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                    mode="outlined"
                    label="Contraseña"
                    returnKeyType="done"
                    value={password.value}
                    onChangeText={text => setPassword({ value: text, error: '' })}
                    error={!!email.error}
                    errorText={email.error}
                    secureTextEntry
                />
                {password.error ? <Text style={styles.error}>{password.error}</Text> : null}
            </View>
            <View style={styles.forgotPassword}>
                <TouchableOpacity onPress={() => navigation.navigate('ForgotPassword')}>
                    <Text style={styles.label}>Olvidaste tu contraseña?</Text>
                </TouchableOpacity>
            </View>
            <PaperButton
                style={[
                    {
                        width: '100%',
                        marginVertical: 10,
                        backgroundColor: theme.colors.primary
                    }
                ]}
                labelStyle={styles.text}
                mode='contained'
                onPress={() => loginHandle()}
            >
                Ingresar
            </PaperButton>
            {/*<View style={styles.row}>
                <Text style={styles.label}>No tienes una cuenta? </Text>
                <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                    <Text style={styles.link}>Crear cuenta.</Text>
                </TouchableOpacity>
            </View>*/}
        </ContentRoot>
    );
};

LoginScreen.navigationOptions = {
    headerShown: false
}

export default LoginScreen;

const styles = StyleSheet.create({
    containerInput: {
        width: '100%',
        marginVertical: 12,
    },
    input: {
        backgroundColor: '#fff',
        borderColor: theme.colors.primary
    },
    error: {
        fontSize: 14,
        color: theme.colors.error,
        paddingHorizontal: 4,
        paddingTop: 4,
    },
    forgotPassword: {
        width: '100%',
        alignItems: 'flex-end',
        marginBottom: 24,
    },
    row: {
        flexDirection: 'row',
        marginTop: 4,
    },
    label: {
        color: theme.colors.secondary,
    },
    link: {
        fontWeight: 'bold',
        color: theme.colors.primary,
    },
});