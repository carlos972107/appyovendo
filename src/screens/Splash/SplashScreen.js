import React from 'react';
import { 
    Text, 
    StyleSheet,
} from 'react-native';
import { theme } from '../../theme';
import { Button as PaperButton } from 'react-native-paper';
import ContentRoot from '../../components/ContentRoot/ContentRoot';

const SplashScreen = ({navigation}) => {
    return (
      <ContentRoot textHeader='¡Yo Vendo!'>
        <Text style={styles.text}>Hola bienvenidos a la app más productiva para tu negocio.</Text>
        <PaperButton
            style={[
                {
                    width: '100%',
                    marginVertical: 10,
                    backgroundColor: theme.colors.primary
                }
            ]}
            mode="contained"
            labelStyle={{color: '#fff'}}
            onPress={()=> navigation.navigate('Login')}
        >
            Iniciar sesion
        </PaperButton>
        <PaperButton
            style={[
                {
                    width: '100%',
                    marginVertical: 5,
                    backgroundColor: theme.colors.secondary
                }
            ]}
            mode="contained"
            labelStyle={{color: '#fff'}}
            onPress={()=> navigation.navigate('Register')}
        >
            Registrarse
        </PaperButton>
      </ContentRoot>
    );
};

SplashScreen.navigationOptions = {
  header: null,
};

export default SplashScreen;

const styles = StyleSheet.create({
  text: {
    fontSize: 16,
    lineHeight: 26,
    color: theme.colors.secondary,
    textAlign: 'center',
    marginBottom: 14,
  },
});