import React from 'react';
import {
  FlatList,
  Text,
  View,
  Image,
  Alert
} from 'react-native';
import { Card, Paragraph, Subheading, Title } from 'react-native-paper';
import { firebase } from '../../firebase/firebase';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './styles';
import AddButton from '../../components/AddButton/AddButtonScreen';
import Loader from '../../components/Loader/Loader';
import { FEATCH_PRODUCT_SELLER } from '../../query';
import { DELETE_PRODUCT } from '../../mutation';
import { Query } from '@apollo/react-components';
import { useMutation } from '@apollo/react-hooks';
import { theme } from '../../theme';
import { formatPrice } from '../../utils';

const RenderProduct = ({ item, navigation }) => {
  return (
    <Card style={styles.container}>
      <Card.Cover style={{ height: 150 }} source={item.photo_url ? { uri: item.photo_url } : require('../../assets/product_default.jpg')} />
      <Card.Content style={{alignContent: 'center', alignItems: 'center'}}>
        <Title style={{textAlign: 'center'}}>{item.name}</Title>
        {item.discount &&
          <Text style={styles.discount}>Descuento: 10%</Text>
        }
        <Subheading style={styles.price}>Valor: {formatPrice(item.price)}</Subheading>
        <Paragraph style={styles.category}>Categoria: {item.category.name || ''}</Paragraph>
        <MutationProduct item={item} navigation={navigation} />
      </Card.Content>
    </Card>
  )
};

const MutationProduct = (props) => {
  const [deleteProduct, {data}] = useMutation( DELETE_PRODUCT );

  const onPressDeleteProduct = (item) => {
    Alert.alert('Eliminar', 'Quiere eliminar este producto para siempre?', [
      {text: 'Aceptar', onPress: () => onPressDelete(item)},
      {text: 'Cancelar'}
    ]);
    return;
  }
  const onPressDelete = async (item) => {
    const { _id, photo_url } = item;
    await deleteStorage(photo_url);
    const { data } = await deleteProduct({ 
      variables: { _id },
      update: (cache) => {
        const { getProductsSeller } = cache.readQuery({ query: FEATCH_PRODUCT_SELLER });
        const products = getProductsSeller.filter(c => c._id !== _id );
        cache.writeQuery({
          query: FEATCH_PRODUCT_SELLER,
          data: { getProductsSeller: products }
        });
    }}).catch(error => {
      const err = error.graphQLErrors.map(error => error.message);
      Alert.alert('Error, ', err[0], [
          {text: 'Ok'}
      ]);
      return;
    });
    if(data.deleteProduct){
      Alert.alert('Eliminado!', 'Correctamente.', [
        {text: 'Ok'}
      ]);
    }
  }

  const deleteStorage = async (refUrl) => {
    const refStorage = firebase.storage().refFromURL(refUrl);
    await refStorage.delete();
  }

  return(
    <View style={{flexDirection: 'row', justifyContent: 'space-between', marginBottom: 3}}>
      <Icon name="pen" size={22} style={{color: theme.colors.primary, paddingHorizontal: 8}} onPress={() => props.navigation.navigate('Product', { item: props.item })} />
      <Icon name="trash-can" size={22} style={{color: theme.colors.error, paddingHorizontal: 8}} onPress={() => onPressDeleteProduct(props.item)} />
    </View>
  )
}

class ProductListScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return{
        title:"¡YO VENDO!, "+ navigation.getParam('title'),
        headerStyle: {
          backgroundColor: theme.colors.primary,
        },
        headerTintColor: '#fff',
        headerRight: () => (
            <AddButton onPress={navigation.getParam('onPressAddProduct')} />
        )
    }
  };

  constructor(props) {
    super(props);
  }

  componentDidMount(){
      this.props.navigation.setParams({ onPressAddProduct: this.onPressProduct })
  }

  onPressProduct = () => {
    this.props.navigation.navigate('Product');
  };

  render() {
    return (
      <View style={{marginBottom: 10}}>
        {/*<Image style={styles.banner} source={{ uri: item.photo_url }} />*/}
        <Query query={FEATCH_PRODUCT_SELLER}>
          {({loading, error, data}) => {
            if(loading) return <Loader />
            if(typeof(data.getProductsSeller) !== 'undefined' && data.getProductsSeller.length){
              return <FlatList
                vertical
                showsVerticalScrollIndicator={false}
                numColumns={2}
                data={data.getProductsSeller}
                renderItem={({item}) => (
                  <View style={{flex: 1, marginLeft: -5}}>
                    <RenderProduct item={item} navigation={this.props.navigation} />
                  </View>
                )}
                keyExtractor={item => `${item._id}`}
              />
            }else{
              return (<Text style={{margin: 15, color: theme.colors.secondary, fontSize: 25, textAlign: 'center', fontWeight: 'bold'}}>
                Sin datos que mostrar...
              </Text>);
            }
          }}
        </Query>
      </View>
    );
  }
}

export default ProductListScreen;