import { StyleSheet, Dimensions } from 'react-native';

// screen sizing
const { width, height } = Dimensions.get('window');
// orientation must fixed
const SCREEN_WIDTH = width < height ? width : height;

const recipeNumColums = 2;
// item size
const RECIPE_ITEM_HEIGHT = 150;
const RECIPE_ITEM_MARGIN = 20;

// 2 photos per width
const styles = StyleSheet.create({
    counterText: {
        fontFamily: 'System',
        fontSize: 36,
        fontWeight: '400',
        color: '#000',
      },
    buttonText: {
        fontFamily: 'System',
        fontSize: 50,
        fontWeight: '300',
        color: '#007AFF',
        marginLeft: 40,
        marginRight: 40,
      },
    counterContainer: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    banner: {
        width: SCREEN_WIDTH - 30,
        height: 150,
        marginLeft: 15,
        marginTop: 15,
        marginBottom: 10,
        borderRadius: 15
    },
    discount: {
        color: 'red',
    },
    price : {
        color: 'blue'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#fff',
        alignItems: 'center',
        marginLeft: RECIPE_ITEM_MARGIN,
        marginTop: 20,
        marginRight: RECIPE_ITEM_MARGIN,
        width: 165,
        borderColor: '#cccccc',
        borderWidth: 0.5,
    },
    photo: {
        width: '100%',
        height: RECIPE_ITEM_HEIGHT,
        borderRadius: 15,
        borderBottomLeftRadius: 0,
        borderBottomRightRadius: 0
    },
    title: {
        flex: 1,
        fontSize: 17,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#444444',
        marginTop: 3,
        marginRight: 3,
        marginLeft: 3,
    },
    category: {
        textAlign: 'center',
        marginTop: 5,
        marginBottom: 5
    }
});
export default styles;