import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');
// orientation must fixed
const SCREEN_WIDTH = width < height ? width : height;

const RECIPE_ITEM_HEIGHT = 150;
const RECIPE_ITEM_MARGIN = 20;


const styles = StyleSheet.create({
    containerShop: {
        padding: 15
    },
    totalPrice: {
        fontSize: 22,
        marginBottom: 15
    },
    countShop: {
        fontSize: 22,
        marginBottom: 15
    },
    seller: {
        fontSize: 22,
        marginBottom: 15
    },
    containerProductShop: {
        width: SCREEN_WIDTH - 25,
        height: 500,
        marginBottom: 15,
    },
    discount: {
        position: 'absolute',
        color: 'red',
        left: 175,
        top: 35
    },
    price : {
        position: 'absolute',
        color: 'blue',
        left: 175,
        top: 55
    },
    photo: {
        width: 150,
        position: 'relative',
        height: RECIPE_ITEM_HEIGHT,
        borderRadius: 15,
        marginBottom: 20
    },
    title: {
        position: 'absolute',
        fontSize: 17,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#444444',
        top: 10,
        left: 175
    },
    category: {
        position: 'absolute',
        top: 85,
        left: 175
    }
});

export default styles;