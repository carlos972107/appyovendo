import React, { Component } from 'react';

import { View, Text, TouchableHighlight, FlatList, Image } from 'react-native';
import { Button } from 'react-native-elements';
import { getRecipes, getCategoryName } from '../../data/MockDataAPI';
import styles from './styles';

export default class ShoppinCartScreen extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: '¿QUIEN VENDE?, CARRITO DE COMPRAS'
        }
    };

    constructor(props) {
        super(props);
    }

    renderCart = ({ item }) => (
        <TouchableHighlight underlayColor='rgba(73,182,77,1,0.9)' onPress={() => {}}>
            <View>
                <Image style={styles.photo} source={{ uri: item.photo_url }} />
                <Text style={styles.title}>{item.title}</Text>
                <Text style={styles.discount}>Descuento: 10%</Text>
                <Text style={styles.price}>Valor: 15.000</Text>
                <Text style={styles.category}>{getCategoryName(item.categoryId)}</Text>
            </View>
        </TouchableHighlight>
    )

    render(){
        const { navigation } = this.props;
        const item = navigation.getParam('categorie');
        const recipesArray = getRecipes(item.id);
        return(
            <View style={styles.containerShop}>
                <Text style={styles.totalPrice}>Valor total: $150.000</Text>
                <Text style={styles.countShop}>Unidades en carrito: #12</Text>
                <Text style={styles.seller}>Tienda: Shoppin tienda</Text>
                <View style={styles.containerProductShop}>
                    <FlatList
                        vertical
                        showsVerticalScrollIndicator={false}
                        data={recipesArray}
                        renderItem={this.renderCart}
                        keyExtractor={item => `${item.recipeId}`}
                    />
                </View>
                <Button title='Realizar compra' onPress={()=>{}} style={styles.buttonRequest} />
            </View>
        )
    }
}