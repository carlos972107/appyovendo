import React, { useEffect } from 'react';
import {
  FlatList,
  TouchableHighlight
} from 'react-native';
import styles from './styles';
import { View } from 'native-base';
import { Card, Text, Title, Subheading, Paragraph } from 'react-native-paper';
import { formatPrice } from '../../utils';
import MenuImage from '../../components/MenuImage/MenuImage';
import { Query } from '@apollo/react-components';
import { useMutation } from '@apollo/react-hooks';
import { FEATCH_REQUEST_ORDER } from '../../query';
import { theme } from '../../theme';
import { Notifications } from 'expo';
import * as Permissions from 'expo-permissions';
import { TOKEN_NOTIFICATION } from '../../mutation';
import { Searchbar } from 'react-native-paper';

const RequestOrdersScreen = (props) => {

  const [createTokenNotification, { data }] = useMutation(TOKEN_NOTIFICATION);
  const [filter, setFilter] = React.useState('');

  const permissionAppPushNotification = async () => {
    const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      return;
    }
    const token = await Notifications.getExpoPushTokenAsync();
    const { data } = await createTokenNotification({ variables: { token } });
    console.log(data);
  }

  useEffect(() => {
    async function featchPermission(){
      await permissionAppPushNotification() 
    }
    featchPermission();
  }, []);

  const renderStateRequest = (state) => {
    let color = '#000';
    if(state === 'VERIFICACION')
      color = '#ffc107';
    if(state === 'ENVIADO')
      color = '#467fd0';
    if(state === 'RECIVIDO')
      color = '#42ba96';

    return <Paragraph style={{color}}>{state}</Paragraph>
  }

  const renderOrders = ({ item }) => (
    <Card style={[styles.container]} onPress={()=>{}}>
      <View style={{flexDirection: 'row'}}>
        <Card.Cover style={{height: 60, width: 60, margin: 20}} source={item.photo_url ? { uri: item.photo_url } : require('../../assets/requests.png')} />        
        <Card.Content>
          <Subheading style={[{color:theme.colors.secondary}]}>Codigo: {item.code}</Subheading>
          <Paragraph style={{color:theme.colors.secondary}}>Estado: <Paragraph style={{fontWeight: '600', color: '#46ee4b'}}>{renderStateRequest(item.state)}</Paragraph></Paragraph>
          <Paragraph style={{color:theme.colors.secondary}}>Total: <Paragraph style={{fontWeight: '600', color: '#46ee4b'}}>$ {formatPrice(item.total)}</Paragraph></Paragraph>
          <Paragraph style={[{color:theme.colors.secondary}]}><Paragraph style={{marginTop: 5}}>Productos: {item.products.length}</Paragraph></Paragraph>
        </Card.Content>
      </View>
    </Card>
  );

    return (
      <View>
        <Searchbar onChange={(text) => setFilter(text.nativeEvent.text) } placeholder="Buscar..." value={filter} style={{margin: 15}}  />
        <Query query={FEATCH_REQUEST_ORDER} pollInterval={5000} variables={{ search: filter }}>
          {({ loading, error, data = []}) => {
            if(loading) return (
            <Text style={styles.contentNull}>
              Cargando datos...
            </Text>)
            data = { getRequestOrderAll: [{
              code: 1239948,
              state: 'VERIFICACION',
              total: '130000',
              products: [{},{},{}],
              paymentMethod: 'CONTRA-ENTREGA'
            },{
              code: 1239949,
              state: 'VERIFICACION',
              total: '35000',
              products: [{},{}],
              paymentMethod: 'DAVIPLATA'
            }]}
            if(typeof(data.getRequestOrderAll) !== 'undefined' && data.getRequestOrderAll.length){
              return (<FlatList
                vertical
                showsVerticalScrollIndicator={false}
                data={data.getRequestOrderAll}
                renderItem={renderOrders}
                keyExtractor={item => `${item._id}`}
              />)
            }else{
              return <Text style={styles.contentNull}>
                  Sin datos que mostrar...
                </Text>
            }
          }}
        </Query>
      </View>
    );
}

RequestOrdersScreen.navigationOptions = ({ navigation }) =>({
  title: '¡YO VENDO!, PEDIDOS',
  headerStyle: {
    backgroundColor: theme.colors.primary,
  },
  headerTintColor: '#fff',
  headerLeft: (
    <MenuImage
      onPress={() => {
        navigation.openDrawer();
      }}
    />
  )
});

export default RequestOrdersScreen;
