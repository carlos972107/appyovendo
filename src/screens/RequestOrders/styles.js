import { StyleSheet, Dimensions } from 'react-native';
import { theme } from '../../theme';
// screen sizing
const { width, height } = Dimensions.get('window');
// orientation must fixed
const SCREEN_WIDTH = width < height ? width : height;

const numColumns = 3;
// item size
const RECIPE_ITEM_HEIGHT = 100;
const RECIPE_ITEM_OFFSET = 10;
const RECIPE_ITEM_MARGIN = RECIPE_ITEM_OFFSET * 2;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    marginHorizontal: RECIPE_ITEM_OFFSET * 1.5,
    marginTop: 20,
    padding: 5
  },
  thumbnail: {
    width: 50,
    height: 50,
    marginTop: 25,
    marginLeft: 15
  },
  titleCard: {
    position: 'relative',
    marginTop: 20,
  },
  title: {
    margin: 10,
    marginBottom: 5,
    color: 'black',
    fontSize: 13,
    textAlign: 'center'
  },
  photo: {
    width: (SCREEN_WIDTH - RECIPE_ITEM_MARGIN) / numColumns - RECIPE_ITEM_OFFSET,
    height: RECIPE_ITEM_HEIGHT,
    borderRadius: 60
  },
  contentNull: {
    margin: 15, color: theme.colors.secondary, fontSize: 25, textAlign: 'center', fontWeight: 'bold'
  }
});

export default styles;
