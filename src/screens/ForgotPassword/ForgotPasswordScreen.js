import React, { useState } from 'react';
import { theme } from '../../theme';
import { TextInput as Input, Button as PaperButton } from 'react-native-paper';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    StyleSheet,
} from 'react-native';
import ContentRoot from '../../components/ContentRoot/ContentRoot';

const ForgotPasswordScreen = ({ navigation }) => {
    const [email, setEmail] = useState({ value: '', error: '' });
  
    const _onSendPressed = () => {
      const emailError = emailValidator(email.value);
      if (emailError) {
        setEmail({ ...email, error: emailError });
        return;
      }
      navigation.navigate('Login');
    };

    const emailValidator = (email) => {
        const re = /\S+@\S+\.\S+/;
        if (!email || email.length <= 0) return 'El campo no puede quedar vacio.';
        if (!re.test(email)) return 'Correo electronico no valido.';
        return '';
    };
  
    return (
        <ContentRoot textHeader='Recuperar mi contraseña.'>
            <View style={styles.containerInput}>
                <Input
                    style={styles.input}
                    selectionColor={''}
                    underlineColor="transparent"
                    theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                    mode="outlined"
                    label="Correo electronico"
                    returnKeyType="next"
                    value={email.value}
                    onChangeText={text => setEmail({ value: text, error: '' })}
                    error={!!email.error}
                    errorText={email.error}
                    autoCapitalize="none"
                    autoCompleteType="email"
                    textContentType="emailAddress"
                    keyboardType="email-address"
                />
                {email.error ? <Text style={styles.error}>{email.error}</Text> : null}
            </View>
            <PaperButton
                    style={[
                        {
                            width: '100%',
                            marginVertical: 10,
                            backgroundColor: theme.colors.primary
                        }
                    ]}
                    labelStyle={styles.text}
                    mode='contained'
                    onPress={() => _onSendPressed()}
                >
                Restablecer contraseña
            </PaperButton>
            <TouchableOpacity
                style={styles.back}
                onPress={() => navigation.navigate('Login')}
                >
                <Text style={styles.label}>← Regresar al inicio de sesion</Text>
            </TouchableOpacity>
        </ContentRoot>
    );
  };

  ForgotPasswordScreen.navigationOptions = {
    headerShown: false
  }
  export default ForgotPasswordScreen;
  const styles = StyleSheet.create({
    back: {
      width: '100%',
      marginTop: 12,
    },
    button: {
      marginTop: 12,
    },
    label: {
      textAlign: 'center',
      color: theme.colors.secondary,
      width: '100%',
    },
    containerInput: {
        width: '100%',
        marginVertical: 12,
    },
    input: {
        backgroundColor: theme.colors.surface,
    },
    error: {
        fontSize: 14,
        color: theme.colors.error,
        paddingHorizontal: 4,
        paddingTop: 4,
    },
  });
  