import React, { useEffect } from 'react';
import {
    Alert,
    StyleSheet,
    TouchableHighlight,
    View
} from 'react-native';
import { TextInput as Input, Button as PaperButton, Avatar } from 'react-native-paper';
import DatePicker from 'react-native-datepicker';
import Loader from '../../components/Loader/Loader';
import { theme } from '../../theme';
import { useMutation, useLazyQuery } from '@apollo/react-hooks';
import { UPDATE_SELLER } from '../../mutation';
import { firebase } from '../../firebase/firebase';
import { FEATCH_USER } from '../../query';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';

const ProfileScreen = (props) => {

    const [businessName, setBusinessName] = React.useState({ value: '' });
    const [initDate, setInitDate] = React.useState({ value: '' });
    const [endDate, setEndDate] = React.useState({ value: '' });
    const [paymentMethods, setPaymentMethods] = React.useState({ value: [] });
    const [phone, setPhone] = React.useState({ value: '' });
    const [mobile, setMobile] = React.useState({ value: '' });
    const [email, setEmail] = React.useState({ value: '' });
    const [loader, setLoader] = React.useState({ value: false });
    const [photo_url, setPhoto] = React.useState({ value: '', prevValue: '' });
    const [_id, setId] = React.useState({value: ''})

    const [updateSeller, {data}] = useMutation(UPDATE_SELLER);
    
    const [getSeller] = useLazyQuery(FEATCH_USER, {
        onCompleted: data => {
            const { getSeller } = data;
            setId({ value: getSeller._id });
            setEmail({ value: getSeller.email });
            setInitDate({ value: getSeller.initDate });
            setEndDate({ value: getSeller.endDate });
            setBusinessName({ value: getSeller.businessName });
            setPhoto({ value: getSeller.photo_url, prevValue: getSeller.photo_url });
            setMobile({ value: getSeller.mobile });
            setPhone({ value: getSeller.phone });
        }
    });

    useEffect(() => {
        const featchBuyer = async () => {
            return await getSeller({});
        }
        featchBuyer();
    }, []);

    const handlerOnPressMod = async () => {
        setLoader({ value: true });
        let photoSave = '';
        if(photo_url.value !== photo_url.prevValue 
            && photo_url.prevValue && photo_url.value){
            await deleteStorage(photo_url.preValue);
            photoSave = await saveStorage(photo_url.value); 
        }else if(photo_url.value){
            photoSave = await saveStorage(photo_url.value); 
        }
        let { data } = await updateSeller({ variables: { 
            businessName: businessName.value,
            initDate: initDate.value,
            endDate: endDate.value,
            phone: phone.value,
            mobile: mobile.value,
            email: email.value,
            paymentMethods: paymentMethods.value,
            photo_url: photoSave,
            _id: _id.value
        }}).catch(async error => {
            setLoader({ value: false });
            Alert.alert('Error!', 'Ocurrio algo inesperado.', [
                {text: 'Ok'}
            ]);
            if(photoSave.length)
                await deleteStorage(photoSave);
            console.log(error);
        });
        if(data.updateBuyer){
            setLoader({ value: false });
            Alert.alert('Modificado!', 'Los datos fueron modificados.', [
                {text: 'Ok'}
            ]);
            return;
        }
    }

    const openGalery = async () => {
        const resultPermission = await Permissions.askAsync(
          Permissions.CAMERA_ROLL
        );
        if(resultPermission){
          const resultImagePicker = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [4, 3]
          });
          if(resultImagePicker.cancelled === false){
            const imageUri = resultImagePicker.uri;
            setPhoto({ value: imageUri });
          }
        }
    }
    
    const uploadImage = uri => {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.onerror = reject;
            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    resolve(xhr.response);
                }
            };

            xhr.open("GET", uri);
            xhr.responseType = "blob";
            xhr.send();
        });
    };
    
    const generateUUID = () => {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    }
    
    const saveStorage = async (imageUri) => {
        const refStorage = firebase.storage().ref();
        return uploadImage(imageUri)
        .then(async res => {
          const task = await refStorage.child(`profile/${generateUUID() + new Date()}`).put(res);
          return await task.ref.getDownloadURL();
        }).catch(error => {
          console.log(error);
          return false;
        });
    }
    
    const deleteStorage = async (refUrl) => {
        const refStorage = firebase.storage().refFromURL(refUrl);
        await refStorage.delete();
    }

    return (
        <View style={styles.background}>
            <View style={styles.container}>
                {loader.value &&
                    <Loader />
                }
                <TouchableHighlight underlayColor='rgba(73,182,77,1, 1)' onPress={() => openGalery()}>
                    <Avatar.Image style={{marginBottom: 15, backgroundColor: 'transparent'}} size={150} source={ photo_url.value ? { uri: photo_url.value } : require('../../assets/seller_default.png') }  />
                </TouchableHighlight>
                <Input
                    style={styles.input}
                    selectionColor={''}
                    theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent'}}}
                    mode="outlined"
                    label="Nombre Negocio"
                    returnKeyType="done"
                    value={businessName.value}
                    onChangeText={text => setBusinessName({ value: text })}
                />

                <Input
                    style={styles.input}
                    selectionColor={''}
                    theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                    mode="outlined"
                    label="Correo electronico"
                    keyboardType='email-address'
                    returnKeyType="done"
                    value={email.value}
                    onChangeText={text => setEmail({ value: text })}
                />

                <DatePicker
                    style={[styles.input, { marginTop: 15 }]}
                    date={initDate.value}
                    mode="time"
                    placeholder="Hora apertura"
                    format="h:mm a"
                    iconSource={null}
                    customStyles={{
                        dateIcon: {
                            position: 'absolute',
                        },
                        dateInput: {
                            flex: 1,
                            padding: 25,
                            borderRadius: 4,
                            borderColor: 'gray',
                            marginBottom: 4,
                            paddingLeft: -4,
                            alignItems: 'flex-start'
                        }
                    }}
                    confirmBtnText="Confirmar"
                    cancelBtnText="Cancelar"
                    onDateChange={(date) => setInitDate({ value: date }) }
                />

                <DatePicker
                    style={[styles.input, { marginTop: 15 }]}
                    date={endDate.value}
                    mode="time"
                    placeholder="Hora cierre"
                    format="h:mm a"
                    iconSource={null}
                    customStyles={{
                        dateIcon: {
                            position: 'absolute',
                        },
                        dateInput: {
                            flex: 1,
                            padding: 25,
                            borderRadius: 4,
                            borderColor: 'gray',
                            marginBottom: 4,
                            paddingLeft: -4,
                            alignItems: 'flex-start'
                        }
                    }}
                    confirmBtnText="Confirmar"
                    cancelBtnText="Cancelar"
                    onDateChange={(date) => setEndDate({ value: date }) }
                />

                <Input
                    style={styles.input}
                    selectionColor={''}
                    theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                    mode="outlined"
                    label="Telefono"
                    keyboardType='numeric'
                    returnKeyType="done"
                    value={phone.value}
                    onChangeText={text => setPhone({ value: text })}
                />

                <Input
                    style={styles.input}
                    selectionColor={''}
                    theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                    mode="outlined"
                    label="Celular"
                    keyboardType='numeric'
                    returnKeyType="done"
                    value={mobile.value}
                    onChangeText={text => setMobile({ value: text })}
                />

                <PaperButton
                    style={[
                        {
                            width: '100%',
                            marginVertical: 10,
                            backgroundColor: theme.colors.primary
                        }
                    ]}
                    labelStyle={styles.text}
                    mode='contained'
                    onPress={() => handlerOnPressMod()}
                >
                    Modificar
                </PaperButton>
            </View>
        </View>
    );
}

ProfileScreen.navigationOptions = {
    title: "¡YO VENDO!, PERFIL",
    headerStyle: {
        backgroundColor: theme.colors.primary,
    },
    headerTintColor: '#fff',
};

export default ProfileScreen;

const styles = StyleSheet.create({
    label: {
        color: theme.colors.secondary,
    },
    button: {
        marginTop: 24,
    },
    row: {
        flexDirection: 'row',
        marginTop: 4,
    },
    link: {
        fontWeight: 'bold',
        color: theme.colors.primary,
    },
    input: {
        width: '100%',
        paddingBottom: 8,
        backgroundColor: '#fff',
        borderBottomColor: '#000'
    },
    background: {
        flex: 1,
        backgroundColor: '#fff',
    },
    container: {
        flex: 1,
        padding: 20,
        width: '100%',
        maxWidth: 340,
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
});