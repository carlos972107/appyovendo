import React from 'react';
import {
  FlatList,
  View,
  Alert
} from 'react-native';
import styles from './styles';
import { firebase } from '../../firebase/firebase';
import { Card, Title, Text, Paragraph } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { FEATCH_CATEGORIES_SELLER } from '../../query';
import { DELETE_CATEGORIE } from '../../mutation';
import { Query } from '@apollo/react-components';
import { useMutation } from '@apollo/react-hooks';
import Loader from '../../components/Loader/Loader';
import AddButton from '../../components/AddButton/AddButtonScreen';
import { theme } from '../../theme';

const MutationCategories = (props) => {
  const [deleteCategory, { data }] = useMutation( DELETE_CATEGORIE );

  const onPressDeleteCategory = (item) => {
    Alert.alert('Eliminar', 'Quiere eliminar esta categoria para siempre?', [
      {text: 'Aceptar', onPress: () => onPressDelete(item)},
      {text: 'Cancelar'}
    ]);
    return;
  }
  const onPressDelete = async (item) => {
    const { _id, photo_url } = item;
    await deleteStorage(photo_url);
    const { data } = await deleteCategory({ 
      variables: { _id },
      update: (cache) => {
        const { getCategoriesAllSeller } = cache.readQuery({ query: FEATCH_CATEGORIES_SELLER });
        const CategorySeller = getCategoriesAllSeller.filter(c => c._id !== _id );
        cache.writeQuery({
          query: FEATCH_CATEGORIES_SELLER,
          data: { getCategoriesAllSeller: CategorySeller }
        });
    }}).catch(error => {
      const err = error.graphQLErrors.map(error => error.message);
      Alert.alert('Error, ', err[0], [
          {text: 'Ok'}
      ]);
      return;
    });
    if(data.deleteCategory){
      Alert.alert('Eliminado!', 'Correctamente.', [
        {text: 'Ok'}
      ]);
    }
  }

  const deleteStorage = async (refUrl) => {
    const refStorage = firebase.storage().refFromURL(refUrl);
    await refStorage.delete();
  }

  return(
    <View style={{flexDirection: 'column', marginTop: 8}}>
      <Icon name="pen" size={22} style={{color: theme.colors.primary}} onPress={() => props.navigation.navigate('Category', { item: props.item })} />
      <Icon name="trash-can" size={22} style={{color: theme.colors.error}} onPress={() => onPressDeleteCategory(props.item)} />
    </View>
  )
}

export default class CategoriesListScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: "¡YO VENDO!, " + navigation.getParam('title'),
      headerStyle: {
        backgroundColor: theme.colors.primary,
      },
      headerTintColor: '#fff',
      headerRight: () => (
        <AddButton onPress={ navigation.getParam('onAddButtonAction') } />
      )
    };
  };

  componentDidMount(){
    this.props.navigation.setParams({ onAddButtonAction: this.onAddButtonAction })
  }

  onAddButtonAction = () => {
    this.props.navigation.navigate('Category');
  }

  constructor(props) {
    super(props);
  }

  renderCategories = ({ item }) => (
    <View style={{ marginTop: 20, marginLeft: 20, marginRight: 20 }}>
      <Card>
        <Card.Cover source={{ uri: item.photo_url ? item.photo_url 
          : 'https://presencia.unah.edu.hn/assets/Uploads/5f6607847a/alimentos-e1549655531380.jpg' }}/>
        <Card.Content>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{flexDirection: 'column'}}>
              <Title>{item.name}</Title>
              <Paragraph>{item.description}</Paragraph>
            </View>
            <MutationCategories item={item} navigation={this.props.navigation} />
          </View>
        </Card.Content>
      </Card>
    </View>
  );

  render(){
    return (
      <View style={{marginBottom: 40}}>
        <Query query={FEATCH_CATEGORIES_SELLER}>
          {({loading, error, data}) => {
            if(loading) return <Loader />;
            if(data.getCategoriesAllSeller){
              return (<FlatList
                vertical
                showsVerticalScrollIndicator={false}
                data={data.getCategoriesAllSeller}
                renderItem={this.renderCategories}
                keyExtractor={item => `${item._id}`}
              />);
            }else{
              return (<Text style={{margin: 15, color: theme.colors.secondary, fontSize: 25, textAlign: 'center', fontWeight: 'bold'}}>
                Sin datos que mostrar...
              </Text>);
            }
          }}
        </Query>
      </View>
    );
  }
}
