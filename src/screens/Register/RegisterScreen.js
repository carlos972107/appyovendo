import React from 'react';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    Alert,
    StyleSheet,
    ScrollView,
} from 'react-native';
import { TextInput as Input, Button as PaperButton } from 'react-native-paper';
import moment from 'moment';
//import Geocoder from 'react-native-geocoding';
import ContentRoot from '../../components/ContentRoot/ContentRoot';
import { theme } from '../../theme';
import DateTimePicker from '@react-native-community/datetimepicker';
import { useMutation } from '@apollo/react-hooks';
import { CREATE_SELLER } from '../../mutation';
//Geocoder.init("AIzaSyBLAo5oS5bU3mXvZ4x6yNkU_t4tBDiL-qc", {language : "en"});

const RegisterScreen = ({navigation}) => {

    const [input, setData] = React.useState({
        businessName: '',
        phone: '',
        mobile: '',
        password: '',
        confirmPassword: '',
        address: {
            lat: '',
            lng: ''
        },
        email: '',
        initDate: '',
        endDate: '',
    });

    const [show, setShow] = React.useState(false)
    const [showEnd, setShowEnd] = React.useState(false)

    const onChange = (event, selectedDate) => {
        const initDate = selectedDate || input.initDate;
        setShow(Platform.OS === 'ios');
        setData({ ...input, initDate});
    };

    const onChangeEnd = (event, selectedDate) => {
        const endDate = selectedDate || input.endDate;
        setShowEnd(Platform.OS === 'ios');
        setData({ ...input, endDate});
    };

    const [createSeller, { data }] = useMutation(CREATE_SELLER);

    const encontrarCoordenadas = () => {
        navigator.geolocation.getCurrentPosition(
            posicion => {
            const coordenadas = posicion;
            input.address = {
                lat: coordenadas.coords.latitude.toString(),
                lng: coordenadas.coords.longitude.toString(),
                zoom: 12
            }
            //goggle maps hay que comprar el uso de la api
            /*Geocoder.from(coordenadas.coords.latitude, coordenadas.coords.longitude).then(location => {
                console.log(location)
            });*/
          },
          error => console.log(error.message),
          { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
    };

    encontrarCoordenadas();

    const handleOnPressRegister = async (inputs) => {
        let { data } = await createSeller({ variables: { 
            businessName: inputs.businessName,
            phone: inputs.phone,
            mobile: [inputs.mobile],
            email: inputs.email,
            password: inputs.password,
            address: inputs.address,
            initDate: inputs.initDate,
            endDate: inputs.endDate,
            priority: 0,
            paymentMethod: [],
            role: 'SELLER',
            state: 'VERIFICACION',
            photo_url: ''
        }});
        if(data.createSeller._id){
            Alert.alert('Creado con exito!', 'Ya puedes iniciar sesion.', [
                {text: 'Ok'}
            ]);
            navigation.goBack();
            return;
        }
    }

    return (
        <ContentRoot textHeader='Formulario de registro'>
            <ScrollView style={{width: '100%'}}>
            <Input
                style={styles.input}
                selectionColor={''}
                theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                mode="outlined"
                label="Nombre empresa"
                returnKeyType="done"
                value={input.businessName}
                onChangeText={text => setData({ businessName: text })}
            />

            <Input
                style={styles.input}
                selectionColor={''}
                theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                mode="outlined"
                label="Correo electronico"
                keyboardType='email-address'
                returnKeyType="done"
                value={input.email}
                onChangeText={text => setData({ email: text })}
            />

            <Input
                style={styles.input}
                selectionColor={''}
                theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                mode="outlined"
                label="Telefono"
                keyboardType='numeric'
                returnKeyType="done"
                value={input.phone}
                onChangeText={text => setData({ phone: text })}
            />

            <Input
                style={styles.input}
                selectionColor={''}
                theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                mode="outlined"
                label="Celular"
                keyboardType='numeric'
                returnKeyType="done"
                value={input.mobile}
                onChangeText={text => setData({ mobile: text })}
            />

            <View style={{ borderRadius: 100, width: '100%'}}>
                <TouchableOpacity onPress={() => setShow(true)} style={styles.inputContainerStyle}>
                {input.initDate ? (
                    <Text style={styles.textStyle}>{moment(input.initDate).format('h:mm A')}</Text>
                ) : (
                    <Text style={styles.placeholderStyle}>{'Hora de apertura'}</Text>
                )}
                </TouchableOpacity>
                    {show && 
                        <DateTimePicker
                            testID="datepicker"
                            value={input.initDate || new Date()}
                            mode={'time'}
                            is24Hour={false}
                            display="default"
                            onChange={onChange}
                            style={{ backgroundColor: 'white' }}
                        />
                    }
            </View>

            <View style={{ borderRadius: 100, width: '100%'}}>
                <TouchableOpacity onPress={() => setShowEnd(true)} style={styles.inputContainerStyle}>
                {input.endDate ? (
                    <Text style={styles.textStyle}>{moment(input.endDate).format('h:mm A')}</Text>
                ) : (
                    <Text style={styles.placeholderStyle}>{'Hora de cierre'}</Text>
                )}
                </TouchableOpacity>
                    {showEnd && 
                        <DateTimePicker
                            testID="datepicker1"
                            value={input.endDate || new Date()}
                            mode={'time'}
                            is24Hour={false}
                            display="default"
                            onChange={onChangeEnd}
                            style={{ backgroundColor: 'white' }}
                        />
                    }
            </View>

            <Input
                style={[styles.input, { marginTop: 3 }]}
                mode="outlined"
                label="Contraseña"
                theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
                returnKeyType="done"
                value={input.password}
                onChangeText={text => setData({ password: text })}
                secureTextEntry
            />

            <PaperButton
                style={[
                    {
                        width: '100%',
                        marginVertical: 10,
                        backgroundColor: theme.colors.primary
                    }
                ]}
                labelStyle={styles.text}
                mode='contained'
                onPress={() => handleOnPressRegister(input)}
            >
                Ingresar
            </PaperButton>

            <View style={styles.row}>
                <Text style={styles.label}>Ya tienes una cuenta? </Text>
                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                <Text style={styles.link}>Iniciar sesion</Text>
                </TouchableOpacity>
            </View>
            </ScrollView>
        </ContentRoot>
    );
};

RegisterScreen.navigationOptions = {
    headerShown: false
}

export default RegisterScreen;

const styles = StyleSheet.create({
    label: {
        color: theme.colors.secondary,
    },
    button: {
        marginTop: 24,
    },
    row: {
        flexDirection: 'row',
        marginTop: 4,
    },
    link: {
        fontWeight: 'bold',
        color: theme.colors.primary,
    },
    input: {
        width: '100%',
        paddingBottom: 8,
        backgroundColor: '#fff',
        borderBottomColor: '#000'
    },
     inputContainerStyle: {
        alignItems: 'flex-start',
        justifyContent: 'center',
        borderWidth: .5,
        borderColor: '#000',
        borderRadius: 5,
        backgroundColor: '#fff',
        marginVertical: 10,
        height: 60,
      },
      placeholderStyle: {
        fontSize: 16,
        width: '100%',
        color: '#9b9b9b',
        marginHorizontal: 10,
      },
      textStyle: {
        color: '#000',
        fontSize: 16,
        marginHorizontal: 10,
      },
});