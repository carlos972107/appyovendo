import React, { useState, useEffect } from 'react';
import {
  ScrollView,
  Alert,
  View,
  Text,
  TouchableHighlight
} from 'react-native';
import styles from './styles';
import { TextInput as Input, Button as PaperButton, Card, Checkbox } from 'react-native-paper';
import Select2 from 'react-native-select-two';
import Loader from '../../components/Loader/Loader';
import { theme } from '../../theme';
import { firebase } from '../../firebase/firebase';
import { formatPrice, validInputValue } from '../../utils';
import { useMutation } from '@apollo/react-hooks';
import { Query } from '@apollo/react-components';
import { CREATE_PRODUCT, UPDATE_PRODUCT } from '../../mutation';
import { FEATCH_CATEGORIES_SELLER, FEATCH_PRODUCT_SELLER } from '../../query';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';

 const ProductScreen = ({ navigation }) => {

  const [name, setName] = useState({ value: '' });
  const [description, setDescription] = useState({ value: '' });
  const [stok, setStok] = useState({ value: '0' });
  const [price, setPrice] = useState({ value: '0' });
  const [category, setCategory] = useState({ value: '', id: '' });
  const [photo_url, setPhoto] = useState({ value: '', preValue: '' });
  const [visible, setVisible] = useState({ value: false });
  const [_id, setId] = useState({ value: '' });
  const [update, setUpdate] = useState({ value: false });
  const [loader, setLoader] = useState({ value: false });

  useEffect(
    () => {
      const item = navigation.getParam('item');
      if(item){
        setId({ value: item._id });
        setStok({ value: item.stoktaking + '' });
        setUpdate({ value: true });
        setPhoto({ value: item.photo_url, preValue: item.photo_url });
        setName({ value: item.name });
        setDescription({ value: item.description });
        setPrice({ value: item.price + '' });
        setVisible({ value: item.visible });
        setCategory({ value: item.category.name, id: item.category._id });
      }
    },
    []
  )

  const [createProduct, cFeatch] = useMutation(CREATE_PRODUCT);
  const [updateProduct, uFeatch] = useMutation(UPDATE_PRODUCT);

  const handlerOnpressAction = async () => {
    setLoader({ value: true });
    if(
      validInputValue(name.value) && 
      validInputValue(description.value) &&
      validInputValue(stok.value) &&
      validInputValue(price.value) &&
      validInputValue(category.value) &&
      validInputValue(photo_url.value)
      ){
      if(!update.value){
        const photoSave = await saveStorage(photo_url.value);
        await onCreateProductAction(photoSave);
      }else{
        if(photo_url.value !== photo_url.preValue){
          await deleteStorage(photo_url.preValue);
          const photoSave = await saveStorage(photo_url.value);
          await onUpdateProductAction(photoSave);
        }else{
          await onUpdateProductAction(photo_url.value);
        }
      }
      
    }
  }

  const onCreateProductAction = async (photoSave) => {
    if(photoSave){
      const { data } = await createProduct({
        variables: {
          photo_url: photoSave,
          name: name.value,
          description: description.value,
          price: parseInt(price.value),
          stoktaking: parseInt(stok.value),
          visible: visible.value,
          category: category.id
        }, update: (cache, { data: { createProduct } }) => {
          const { getProductsSeller } = cache.readQuery({ query: FEATCH_PRODUCT_SELLER });
          cache.writeQuery({
            query: FEATCH_PRODUCT_SELLER,
            data: { getProductsSeller: [...getProductsSeller, createProduct] }
          });
        }
      }).catch(error => {
        const err = error.graphQLErrors.map(error => error.message);
        setLoader({ value: false });
        deleteStorage(photoSave);//eliminar la imagen ya guardada
        Alert.alert('Error, ', err[0], [
            {text: 'Ok'}
        ]);
        return;
      });
      if(data.createProduct){
        setLoader({ value: false });
        Alert.alert('Guardado!', 'Todos los datos fueron guardados correctamente.', [
          {text: 'Ok'}
        ]);
      }
      setName({ value: '' });
      setDescription({ value: '' });
      setPrice({ value: 0 });
      setStok({ value: 0 });
      setPhoto({ value: '' });
      setVisible({ value: false });
      setCategory({ value: '', id: '' });
    }
  }

  const onUpdateProductAction = async (photoSave) => {
    if(photoSave){
      const { data } = await updateProduct({
        variables: {
          photo_url: photoSave,
          name: name.value,
          description: description.value,
          price: parseInt(price.value),
          stoktaking: parseInt(stok.value),
          visible: visible.value,
          category: category.id,
          _id: _id.value
        }, update: (cache) => {
          const { getProductsSeller } = cache.readQuery({ query: FEATCH_PRODUCT_SELLER });
          const newProduct = getProductsSeller.map(p => {
            if(p._id === _id.value){
              const { __typename, _id } = p;
              return {
                __typename,
                _id,
                photo_url: photoSave,
                name: name.value,
                description: description.value,
                price: parseInt(price.value),
                stoktaking: parseInt(stok.value),
                visible: visible.value,
                category: {
                  __typename: p.category.__typename,
                  _id: p.category._id,
                  name: category.value
                }
              }
            }else
              return p;
          })
          cache.writeQuery({
            query: FEATCH_PRODUCT_SELLER,
            data: { getProductsSeller: newProduct }
          });
        }
      }).catch(error => {
        const err = error.graphQLErrors.map(error => error.message);
        setLoader({ value: false });
        Alert.alert('Error, ', err[0], [
            {text: 'Ok'}
        ]);
        return;
      });
      if(data.updateProduct){
        setLoader({ value: false });
        Alert.alert('Guardado!', 'Todos los datos fueron modificados correctamente.', [
          {text: 'Ok'}
        ]);
      }
      navigation.goBack();
    }
  }

  const openGalery = async () => {
    const resultPermission = await Permissions.askAsync(
      Permissions.CAMERA_ROLL
    );
    if(resultPermission){
      const resultImagePicker = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [4, 3]
      });
      if(resultImagePicker.cancelled === false){
        const imageUri = resultImagePicker.uri;
        setPhoto({ value: imageUri });
      }
    }
  }

  const uploadImage = uri => {
    return new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest();
      xhr.onerror = reject;
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          resolve(xhr.response);
        }
      };

      xhr.open("GET", uri);
      xhr.responseType = "blob";
      xhr.send();
    });
  };

  const generateUUID = () => {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  }

  const saveStorage = async (imageUri) => {
    const refStorage = firebase.storage().ref();
    return uploadImage(imageUri)
    .then(async res => {
      const task = await refStorage.child(`images/${generateUUID() + new Date()}`).put(res);
      return await task.ref.getDownloadURL();
    }).catch(error => {
      console.log(error);
      return false;
    });
  }

  const deleteStorage = async (refUrl) => {
    const refStorage = firebase.storage().refFromURL(refUrl);
    await refStorage.delete();
  }

  const mapCategories = (data) => {
    let rows = [];
    data.map((item, i) => {
      rows.push({ name: item.name, id: item._id })
    });
    return rows;
  }

  return (
    <ScrollView style={styles.mainContainer}>
      {loader.value &&
        <Loader />
      }
      <View style={{ marginTop: 20, marginLeft: 20, marginRight: 20 }}>
        <TouchableHighlight underlayColor='rgba(73,182,77,1, 1)' onPress={() => openGalery()}>
          <Card.Cover style={{ marginTop: 25, borderRadius: 5, borderWidth: 0.5, borderColor: 'gray' }} height={200} source={photo_url.value ? { uri: photo_url.value } : require('../../assets/product_default.jpg')} />
        </TouchableHighlight>
        <Input
          style={[styles.input]}
          theme={{ colors: { primary: theme.colors.primary, underlineColor:'transparent' }}}
          mode="outlined"
          label="Nombre producto"
          returnKeyType="done"
          autoFocus={true}
          value={name.value}
          onChangeText={text => setName({ value : text })}
        />

        <Input
          style={[styles.input]}
          theme={{ colors: { primary: theme.colors.primary, underlineColor:'transparent' }}}
          mode="outlined"
          label="Precio"
          keyboardType='numeric'
          returnKeyType="done"
          value={formatPrice(price.value)}
          onChangeText={text => setPrice({ value : text })}
        />

        <Input
          style={[styles.input]}
          theme={{ colors: { primary: theme.colors.primary, underlineColor:'transparent' }}}
          mode="outlined"
          label="Existencias"
          keyboardType='numeric'
          returnKeyType="done"
          value={stok.value}
          onChangeText={text => setStok({ value : text })}
        />

        <Input
            style={styles.input}
            theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent' }}}
            mode="outlined"
            label="Descripcion"
            returnKeyType="done"
            multiline={true}
            numberOfLines={8}
            value={description.value}
            onChangeText={text => setDescription({ value: text })}
        />

        <View style={{marginTop: 10, flexDirection: 'row'}}>
          <Checkbox
            color={theme.colors.primary}
            status={visible.value ? 'checked' : 'unchecked'}
            onPress={() => {
              setVisible({ value: !visible.value });
            }}
          />
          <Text style={{marginTop: 7, fontSize: 18}}>{visible.value ? 'Visible para los compradores' : 'No está visible para los compradores'}</Text>
        </View>

        <Query query={FEATCH_CATEGORIES_SELLER}>
          {({loading, error, data}) => {
            if(loading) return <Text>un momento por favor...</Text>;
            if(data.getCategoriesAllSeller){
              const items = mapCategories(data.getCategoriesAllSeller);
            return (
              <Input
                style={styles.input}
                mode='outlined'
                render={() => (
                  <Select2
                    isSelectSingle
                    style={{ borderRadius: 4, marginTop: 4, borderColor: 'transparent' }}
                    popupTitle="Seleccionar categoria"
                    searchPlaceHolderText='Seleccionar categoria'
                    colorTheme={theme.colors.primary}
                    cancelButtonText='Cancelar'
                    selectButtonText='Guardar'
                    title="Seleccionar categoria"
                    value={category.value}
                    data={items}
                    onSelect={(id, item) => {
                      if(item.length)
                        setCategory({ value: item[0].name, id: id[0] })
                      else if(!item)
                        setCategory({ value: '', id: '' });
                    }}
                    onRemoveItem={res => {
                      console.log(res[0])
                    }}
                  />
                )}
              />
              )
            }
          }}
        </Query>
        <PaperButton
          style={[
            {
                width: '100%',
                marginTop: 22,
                backgroundColor: '#EF7C21',
                marginVertical: 10
            }
          ]}
          labelStyle={styles.text}
          mode='contained'
          onPress={ () => handlerOnpressAction() }
        >
          {!update.value ? 'Guardar' : 'Actualizar'}
        </PaperButton>
        <PaperButton
          style={[
              {
                width: '100%',
                backgroundColor: theme.colors.error,
                marginVertical: 10
              }
          ]}
          labelStyle={styles.text}
          mode='contained'
          onPress={() => navigation.goBack()}
        >
          Cancelar
        </PaperButton>
      </View>
    </ScrollView>
  );
}

ProductScreen.navigationOptions = {
  title: '¡YO VENDO!, MIS PRODUCTOS',
  headerStyle: {
    backgroundColor: theme.colors.primary,
  },
  headerTintColor: '#fff'
};

export default ProductScreen;