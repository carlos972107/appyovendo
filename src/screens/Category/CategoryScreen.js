import React, { useEffect } from 'react';
import {
  View, Alert, TouchableHighlight
} from 'react-native';
import { Card, Button as PaperButton, TextInput as Input, } from 'react-native-paper';
import styles from './styles';
import { firebase } from '../../firebase/firebase';
import { useMutation } from '@apollo/react-hooks';
import { CREATE_CATEGORIE, UPDATE_CATEGORY } from '../../mutation';
import { FEATCH_CATEGORIES_SELLER } from '../../query';
import { theme } from '../../theme';
import Loader from '../../components/Loader/Loader';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';

const CategoryScreen = ({ navigation }) => {

  const [name, setName] = React.useState({ value: '' });
  const [description, setDescription] = React.useState({ value: '' });
  const [photo_url, setPhoto] = React.useState({ value: '', preValue: '' });
  const [loader, setLoader] = React.useState({ value: false });
  const [update, setUpdate] = React.useState({ value: false });
  const [_id, setId] = React.useState({ value: '' });

  useEffect(
    () => {
      const data = navigation.getParam('item');
      if(data){
        setId({ value: data._id });
        setName({ value: data.name });
        setDescription({ value: data.description });
        setPhoto({ value: data.photo_url, preValue: data.photo_url });
        setUpdate({ value: true });
      }
    },
    []
  );

  const [createCategory, Create] = useMutation(CREATE_CATEGORIE);
  const [updateCategory, Update] = useMutation(UPDATE_CATEGORY);

  const uploadImage = uri => {
    return new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest();
      xhr.onerror = reject;
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          resolve(xhr.response);
        }
      };

      xhr.open("GET", uri);
      xhr.responseType = "blob";
      xhr.send();
    });
  };

  const openGalery = async () => {
    const resultPermission = await Permissions.askAsync(
      Permissions.CAMERA_ROLL
    );
    if(resultPermission){
      const resultImagePicker = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [4, 3]
      });
      if(resultImagePicker.cancelled === false){
        const imageUri = resultImagePicker.uri;
        setPhoto({ value: imageUri, preValue: photo_url.preValue });
      }
    }
  }

  const generateUUID = () => {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return uuid;
  }

  const saveStorage = async (imageUri) => {
    const refStorage = firebase.storage().ref();
    return uploadImage(imageUri)
    .then(async res => {
      const task = await refStorage.child(`images/${generateUUID() + new Date()}`).put(res);
      return await task.ref.getDownloadURL();
    }).catch(error => {
      console.log(error);
    });
  }

  const deleteStorage = async (refUrl) => {
    const refStorage = firebase.storage().refFromURL(refUrl);
    await refStorage.delete();
  }

  const onCreateCategory = async (storage) => {
    if(storage){
      const { data } = await createCategory({ variables: {
          name: name.value,
          description: description.value,
          photo_url: storage
        }, update: (cache, { data: { createCategory } }) => {
          const { getCategoriesAllSeller } = cache.readQuery({ query: FEATCH_CATEGORIES_SELLER });
          cache.writeQuery({
            query: FEATCH_CATEGORIES_SELLER,
            data: { getCategoriesAllSeller: [...getCategoriesAllSeller, createCategory] }
          });
        }
      }).catch(error => {
        const err = error.graphQLErrors.map(error => error.message);
        setLoader({ value: false });
        Alert.alert('Error, ', err[0], [
            {text: 'Ok'}
        ]);
        return;
      });
      if(data.createCategory){
        setLoader({ value: false });
        Alert.alert('Guardado!', 'Todos los datos fueron guardados correctamente.', [
          {text: 'Ok'}
        ]);
      }
      setName({ value: '' });
      setDescription({ value: '' });
      setPhoto({ value: '' });
    }
  }

  const onUpdateCategory = async (storage) => {
    if(storage){
      const { data } = await updateCategory({ variables: {
          name: name.value,
          description: description.value,
          photo_url: storage,
          _id: _id.value
        }, 
        update: (cache, { data }) => {
          const { getCategoriesAllSeller } = cache.readQuery({ query: FEATCH_CATEGORIES_SELLER });
          const newCategorieSellers = getCategoriesAllSeller.map(c => {
            if(c._id === _id.value){
              const { __typename, _id } = c;
              return {
                name: name.value,
                description: description.value,
                photo_url: storage,
                _id,
                __typename
              };
            }else
              return c;
          });
          cache.writeQuery({
            query: FEATCH_CATEGORIES_SELLER,
            data: { getCategoriesAllSeller: newCategorieSellers }
          });
        }
      }).catch(error => {
        const err = error.graphQLErrors.map(error => error.message);
        setLoader({ value: false });
        Alert.alert('Error, ', err[0], [
            {text: 'Ok'}
        ]);
        return;
      });
      if(data.updateCategory){
        setLoader({ value: false });
        Alert.alert('Modificado!', 'Todos los datos fueron modificados correctamente.', [
          {text: 'Ok'}
        ]);
      }
      navigation.goBack();
    }
  }

  const handlerOnPressCategory = async () => {
    setLoader({ value: true });
    if(update.value){
      if(photo_url.value !== photo_url.preValue){
        await deleteStorage(photo_url.preValue);
        const storage = await saveStorage(photo_url.value);
        await onUpdateCategory(storage);
      }else{
        await onUpdateCategory(photo_url.value);
      }
    }else{
      const storage = await saveStorage(photo_url.value);
      await onCreateCategory(storage);
    }
    
  }

  return (
    <View style={styles.mainContainer}>
      {loader.value && 
        <Loader />
      }
      <View style={{ marginTop: 20, marginLeft: 20, marginRight: 20 }}>
        <TouchableHighlight underlayColor='rgba(73,182,77,1, 1)' onPress={() => openGalery()}>
          <Card.Cover style={{ marginTop: 25, borderRadius: 5 }} height={200} source={{ uri: photo_url.value ? photo_url.value : 'https://presencia.unah.edu.hn/assets/Uploads/5f6607847a/alimentos-e1549655531380.jpg' }} />
        </TouchableHighlight>
        <Input
            style={[styles.input]}
            theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
            mode="outlined"
            label="Nombre categoria"
            returnKeyType="done"
            autoFocus={true}
            value={name.value}
            onChangeText={text => setName({ value : text })}
        />
        <Input
            style={styles.input}
            theme={{ colors: { primary: theme.colors.primary ,underlineColor:'transparent',}}}
            mode="outlined"
            label="Descripcion"
            returnKeyType="done"
            multiline={true}
            numberOfLines={8}
            value={description.value}
            onChangeText={text => setDescription({ value: text })}
        />
        <PaperButton
          style={[
            {
                width: '100%',
                marginTop: 22,
                backgroundColor: '#EF7C21',
                marginVertical: 10
            }
          ]}
          labelStyle={styles.text}
          mode='contained'
          onPress={ () => handlerOnPressCategory() }
        >
          {!update.value ? 'Guardar' : 'Actualizar'}
        </PaperButton>
        <PaperButton
          style={[
              {
                  width: '100%',
                  backgroundColor: theme.colors.error,
                  marginVertical: 10
              }
          ]}
          labelStyle={styles.text}
          mode='contained'
          onPress={() => navigation.goBack()}
        >
          Cancelar
        </PaperButton>
      </View>
    </View>
  );
}

CategoryScreen.navigationOptions = {
  title: '¡YO VENDO!, INGRESAR CATEGORIAS',
  headerStyle: {
    backgroundColor: theme.colors.primary,
  },
  headerTintColor: '#fff'
};

export default CategoryScreen;