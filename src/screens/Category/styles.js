import { StyleSheet } from 'react-native';

import { theme } from '../../theme';

const styles = StyleSheet.create({
  mainContainer: {
    width: '100%',
    height: '100%',
    paddingLeft: 12,
    paddingRight: 12,
    backgroundColor: '#fff'
  },
  input: {
    width: '100%',
    paddingBottom: 8,
    top: 10,
    backgroundColor: '#fff',
  },
});

export default styles;
