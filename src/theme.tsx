import { DefaultTheme } from 'react-native-paper';

export const theme = {
  colors: {
    primary: '#EF7C21',
    secondary: '#423089',
    error: '#f13a59',
  },
};