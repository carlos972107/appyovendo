import gql from 'graphql-tag';

export const FEATCH_SELLER = gql`
    {
        getSellerAll{
            _id
            businessName
            initDate
            endDate
        }
    }
`;

export const FEATCH_CATEGORIES_SELLER = gql`
    {
        getCategoriesAllSeller{
            _id
            photo_url
            name
            description
        }
    }
`;

export const FEATCH_PRODUCT_SELLER = gql`
    {
        getProductsSeller{
            _id
            photo_url
            name
            stoktaking
            price
            description
            visible
            category{
                _id
                name
            }
        }
    }
`;

export const FEATCH_USER = gql`
    {
        getSeller{
            _id
            businessName
            photo_url
            email
            initDate
            endDate
            phone
            mobile
            paymentMethods
        }
    }
`;

export const FEATCH_REQUEST_ORDER = gql`
    query getRequestOrderAll($search: String){
        getRequestOrderAll(search: $search){
            _id
            products
            seller
            buyer
            state
            total
            paymentMethod
        }
    }
`;