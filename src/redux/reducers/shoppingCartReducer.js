const initialState = {
    products: [],
    seller: {},
    filter: false,
};

const shoppingCartReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'ADD_PRODUCT': {
            const product = state.products.find(product => product._id === action.product._id);
            product.length ? product.quantity += 1
            : state.products.push({product: action.product, quantity: 1});
            return {
                ...state,
                products: state.products,
                seller: action.seller
            }
        }
        case 'DEC_PRODUCT': {
            const product = state.products.find(product => product._id === action.product._id);
            product.quantity -= 1;
            return {
                ...state,
                products: state.products,
                seller: action.seller
            }
        }
        case 'POP_PRODUCT': {
            const products = state.products.filter(product => product._id !== action._id);
            state.products = products;
            return {
                ...state,
                products: state.products,
                seller: state.seller
            }
        }
        case 'FILT_PRODUCT': {
            return {
                ...state,
                filter: state.products.find(product => product._id === action._id) ? true : false
            }
        }
        default: {
            return state;
        }
    }
};
export default shoppingCartReducer;