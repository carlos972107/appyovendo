// Initial State
const initialState = {
    stateAuth: {},
};
const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'LOGIN': {
            return {
                ...state,
                stateAuth: action.stateAuth,
            }
        }
        default: {
            return state;
        }
    }
};
export default authReducer;